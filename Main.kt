fun main(args: Array<String>) {
    println("Hello World!")
    println(max(1,2))

    val przykString:String = "Prrrr"
    val przykInt:Int = 42
    val yearsToCompute = 7.56e6
    //val - niemutowalne
    //var - mutowalne

    val name = "Jakub"
    println("Czesc, $name !")

    //val per:Person = Person("Bartek",true)
    //println(per.isMarried)
    //println(per.name)
    whenExample("Andrzej")
    whenExample(imie = "PRR")
    lambdaExample()
    lambdaReturnTypeExample()
    lambdaArgumentsEx()

    //Rectangle class
    val rec = Rectangle(10,5)
    println(rec.area)
    println(rec.name)



    zad2_2()
    zad2_3()
    zad2_4()
    zad2_5()
    zad2_6()
}

fun whenExample(imie:String){
    val studentOp = when(imie){
        "Andrzej"-> "Very good"
        else -> "Not good"
    }

    println("ocena: $studentOp")
}

fun lambdaExample(){
    val imie = "Jakub"
    var dlugoscImienia = imie.count()
    println("Dlugosc imienia: $dlugoscImienia")
    //override count by lambda
    dlugoscImienia = imie.count{letter -> letter == 'J'}
    println("Dlugosc imienia: $dlugoscImienia")
}

fun lambdaReturnTypeExample(){
    val helloFunc:() -> String = {
        val currYear = 2019
        "Welcome: $currYear"
    }
    println(helloFunc())
}

fun lambdaArgumentsEx(){
    val hell:(String,Int) -> String = {
        prr,num ->
        "hello: $prr,   $num"
    }

    println(hell("qqq",123))
}

fun max (a: Int,b:Int) : Int{
    return if(a > b) a else b
}

//example class with getter and setter
//val in parameters allow us to create new field in the same line
class Rectangle(val width:Int,val height:Int){
    val area:Int
    get() = this.width * this.height

    var name:String
    get() = "prr"
    set(value:String){
        name = value
    }

    //constructor dodatkowy

    constructor(width: Int,height: Int,_imie: String):this(width,height){
        name = _imie;
    }
}





//LAB 2
fun zad2_2(){
    val studentStatus:(String,String) -> Unit = { nam:String,auraColour:String -> println("$nam has a $auraColour face color") }
    studentStatus("Jakub","Czerwony")
}

fun zad2_3(){
    var s = Student()
    println("Imie w obiekcie s: ${s.name}")
}

//probb good
class Student(){
    var name:String = "jakub"
    get() = field.capitalize()
}

fun zad2_4(){
    val s = Student1("Janek","PRR")
    println("dane: imie ${s.name} i university: ${s.university}")
}

class Student1(_name:String,var university:String = "uni"){
    var name:String = _name
        get() = field.capitalize()
}

fun zad2_5(){
    val s = Student2("Przyklad")
    s.showStudent()
}

class Student2(_name:String,private var university:String = "uni"){
    private var name:String = _name
        get() = field.capitalize()

    public val showStudent = { println("${this.name}       ${this.university}")}
}

fun zad2_6(){
    val s = Student3("JJJ","XXX")
    s.showStudent();
}

open class Person(protected var name:String){

}

class Student3(_name:String,protected var university:String) : Person(_name){
    public val showStudent = { println("${this.name}       ${this.university}")}
}